from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            expensecategory = form.save(False)
            expensecategory.owner = request.user
            form.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            form.save()
            return redirect("accounts_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/categories/create.html", context)


@login_required
def categories_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories_list": categories,
    }
    return render(request, "receipts/category.html", context)


@login_required
def accounts_list(request):
    accounts = Account.objects.filter()
    context = {
        "accounts_list": accounts,
    }
    return render(request, "receipts/accounts.html", context)
