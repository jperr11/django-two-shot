# Generated by Django 4.2.7 on 2023-11-03 01:30

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0004_alter_account_name_alter_expensecategory_name"),
    ]

    operations = [
        migrations.AlterField(
            model_name="expensecategory",
            name="name",
            field=models.CharField(max_length=50),
        ),
    ]
