from django.urls import path
from accounts.views import my_login, my_logout, signup

urlpatterns = [
    path("login/", my_login, name="login"),
    path("logout/", my_logout, name="logout"),
    path("signup/", signup, name="signup"),
]
